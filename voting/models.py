import datetime
from django.db import models
from registration.models import User
#start
class Poll(models.Model):
    title = models.CharField(max_length=50,blank=False)
    question_outline = models.CharField(max_length=50)
    description = models.CharField(max_length=250)
    pollCriteria_domain=models.CharField(max_length=250,null=True)
    pollCriteria_emails= models.CharField(max_length=250,null=True)
    pollCriteria_phoneNo= models.CharField(max_length=250,null=True)
    startTime = models.DateTimeField(null=True,blank=True)
    endTime   = models.DateTimeField( null=True,blank=True)
    createdBy= models.ForeignKey(User, on_delete=models.DO_NOTHING,null=True)
    def __str__(self):
        return self.id+"--"+self.title +"--"

class Options(models.Model):
    pollID =  models.ForeignKey(Poll, on_delete=models.CASCADE)
    title = models.CharField(max_length=50,blank=False)
    description = models.CharField(max_length=250)
    voteCount=models.IntegerField(default=0)
    def __str__(self):
        return self.id+"--"+self.title +"--"

class VoterList(models.Model):
    pollID =  models.ForeignKey(Poll, on_delete=models.CASCADE)
    userID = models.ForeignKey(User, on_delete=models.DO_NOTHING)
    #set blank=False changed in database manually
    optionID = models.ForeignKey(Options, on_delete=models.CASCADE,null=True,blank=False)
    isVoteCasted=models.BooleanField(default=False)

    def __str__(self):
        return self.id+"--"+self.userID +"--"+self.pollID+"--"+str(self.isVoteCasted)


from pytz import UTC


class Notification(models.Model):
    userId = models.IntegerField()
    type = models.CharField(max_length=10)
    pollId = models.IntegerField()
    createdOn = models.CharField(max_length=50, default=datetime.datetime.now().isoformat())