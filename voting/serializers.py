from rest_framework import serializers
from .models import Poll, Options, VoterList, Notification


class PollSerializer(serializers.ModelSerializer):
    class Meta:
        model = Poll
       # fields = ('email', 'generateCode','verificationStatus')
        fields = '__all__'

class OptionsSerializer(serializers.ModelSerializer):
    class Meta:
        model = Options
        fields = '__all__'

class VoterListSerializer(serializers.ModelSerializer):
    class Meta:
        model = VoterList
        fields = '__all__'

class NotificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = ('userId', 'type', 'pollId', 'createdOn')
