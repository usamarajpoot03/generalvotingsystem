import datetime

import dateutil.parser
from django.shortcuts import render
from django.http import HttpResponse, Http404
from rest_framework import generics
from rest_framework.utils import json
from rest_framework.views import APIView, Response,status
from .models import Poll, Options, VoterList, Notification
from .serializers import PollSerializer, OptionsSerializer, VoterListSerializer, NotificationSerializer
from registration.models import EmailVerification,User,PhoneNoVerification
# Create your views here.
def index(request):
    return HttpResponse("HELLO WORKING-Voting")

class CreatePoll(APIView):
    def get(self, request, format=None):
        allObjects = Poll.objects.all()
        serializer = PollSerializer(allObjects, many=True)
        return Response(serializer.data)
    def post(self, request, format=None):

        receivedData = json.loads(request.body)
        isAdminAllowedForVoting=receivedData['isAdminAllowedForVoting']
        if isAdminAllowedForVoting=="TRUE":
            isAdminAllowedForVotingVal=True
        else:
            isAdminAllowedForVotingVal=False
        del receivedData['isAdminAllowedForVoting']
        #return HttpResponse("isAdminAllowedForVoting: "+ str(isAdminAllowedForVoting))
        serializer = PollSerializer(data=receivedData)
        if serializer.is_valid():
            serializer.save()
            receivedData['id']=serializer.data['id']
            isAdded = createVoterList(receivedData,isAdminAllowedForVotingVal)
            #return createVoterList(receivedData)
            if isAdded == False:
                return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


class DeletePoll(APIView):

    def get(self, request,pk, format=None):
        allObjects = Poll.objects.all()
        serializer = PollSerializer(allObjects, many=True)
        return Response(serializer.data)
    def delete(self, request,pk, format=None):
        # receivedData = json.loads(request.body)
        try:
            obj = Poll.objects.get(id=pk)
            obj.delete()
            try:
                objOptions = Options.objects.filter(pollID_id=pk)
                objOptions.delete()
            except Options.DoesNotExist:
                pass
            try:
                vList = VoterList.objects.filter(pollID_id=pk)
                vList.delete()
                #shahwar Code
                deleteNotificaation(pk)
                return Response({'message': 'Poll Deleted'})
            except VoterList.DoesNotExist:
                pass
        except Poll.DoesNotExist:
            return Response({'message':'Invalid Poll ID'})

def deleteNotificaation(pId):
    Notification.objects.filter(pollId=pId).delete()


def createVoterList(data,isAdminAllowedForVoting):
    if isAdminAllowedForVoting==True:
        voter = {
            "isVoteCasted": False,
            "optionID": None,
            "pollID": data['id'],
            "userID": data['createdBy']}

        serializer = VoterListSerializer(data=voter)
        if serializer.is_valid():
            serializer.save()
            #shahwar work
            createNotification(data['id'], data['createdBy'])
            #shahwar work
        else:
            return False

    if data['pollCriteria_emails']!=None:
        emails=str(data['pollCriteria_emails']).split(',')
        for email in emails:
            try:
                verificaationID = EmailVerification.objects.get(email=email).pk
                userID=User.objects.get(email=verificaationID).id
            except:
                continue
            try:
                alreadyExistRecord = VoterList.objects.filter(pollID=data['id']).get(userID=userID)
                continue
            except:
                pass
            voter={
                   "isVoteCasted":False,
                   "optionID":None,
                   "pollID":data['id'],
                   "userID":userID}
            if (data['createdBy'] == userID) and (not isAdminAllowedForVoting):
                continue
            serializer= VoterListSerializer(data=voter)
            if serializer.is_valid():
                serializer.save()
                # shahwar Work
                createNotification(data['id'], userID)
                # shahwar Work
            else:
                return False
    if data['pollCriteria_domain']!=None:
        emailDomains = str(data['pollCriteria_domain']).split(',')
        allObjects=EmailVerification.objects.all()
        for domain in emailDomains:
            for obj in allObjects:
                if domain in obj.email:
                    #return HttpResponse(domain + ':' + obj.email + '<br>')
                    try:
                        userID = User.objects.get(email=obj.id).id
                    except:
                        continue
                        #already exist in voter List
                    try:
                        alreadyExistRecord = VoterList.objects.filter(pollID=data['id']).get(userID=userID)
                        continue
                    except:
                        pass

                    voter = {
                        "isVoteCasted": False,
                        "optionID": None,
                        "pollID": data['id'],
                        "userID": userID}

                    if (data['createdBy'] == userID) and (not isAdminAllowedForVoting):
                        continue
                    serializer = VoterListSerializer(data=voter)
                    if serializer.is_valid():
                        serializer.save()
                        # shahwar Work
                        createNotification(data['id'], userID)
                        # shahwar Work
                    else:
                        return False
    if data['pollCriteria_phoneNo']!=None:
        phoneNos = str(data['pollCriteria_phoneNo']).split(',')
        allObjects=PhoneNoVerification.objects.all()
        for phNo in phoneNos:
            for obj in allObjects:
                if phNo in obj.phoneNo:
                    #return HttpResponse(domain + ':' + obj.email + '<br>')
                    try:
                        userID = User.objects.get(phoneNo=obj.id).id
                    except:
                        continue
                        #already exist in voter List
                    try:
                        alreadyExistRecord = VoterList.objects.filter(pollID=data['id']).get(userID=userID)
                        continue
                    except:
                        pass

                    voter = {
                        "isVoteCasted": False,
                        "optionID": None,
                        "pollID": data['id'],
                        "userID": userID}

                    if (data['createdBy'] == userID) and (not isAdminAllowedForVoting):
                        continue
                    serializer = VoterListSerializer(data=voter)
                    if serializer.is_valid():
                        serializer.save()
                        # shahwar Work
                        createNotification(data['id'], userID)
                        # shahwar Work
                    else:
                        return False
    return True

def createNotification(pollId, userId):
    notifyObj = {
        "userId": userId,
        "type": "pollAdd",
        "pollId": pollId,
        "createdOn": datetime.datetime.now().isoformat()
    }
    notifySerializer = NotificationSerializer(data=notifyObj)
    if notifySerializer.is_valid():
        notifySerializer.save()
    else:
        return False

class AddOptions(APIView):

    def get(self, request,pollID, format=None):
        allObjects = Options.objects.filter(pollID=pollID)
        serializer = OptionsSerializer(allObjects, many=True)
        return Response(serializer.data)

    def post(self, request,pollID, format=None):
        receivedData = json.loads(request.body)
        receivedData['voteCount']=0
        serializer = OptionsSerializer(data=receivedData)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    def put(self, request,pollID, format=None):
        receivedData = json.loads(request.body)
        userID = receivedData["userID"]
        del receivedData["userID"]
        try:
            obj= Options.objects.get(pk=receivedData['id'])
            receivedData['voteCount']=obj.voteCount+1
            serializer = OptionsSerializer(obj,data=receivedData)
            if serializer.is_valid():
                try:
                    obj = VoterList.objects.get(userID=userID, pollID=receivedData['pollID'])
                except VoterList.DoesNotExist:
                    return Response({'message':"NOT A VALID USER FOR VOTING"})

                if obj.isVoteCasted == True:
                    return Response({'message':"Vote ALREADY CASTED"})
                voter = {
                        "id": obj.id,
                        "isVoteCasted": True,
                        "pollID": int(obj.pollID.id)   ,
                        "userID": obj.userID.id,
                        "optionID":int(receivedData['id'])
                    }
                voterListsrlz = VoterListSerializer(obj, data=voter)
                (voterListsrlz.is_valid())
                voterListsrlz.save()


                serializer.save()
                return Response(serializer.data, status=status.HTTP_201_CREATED)
            return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

        except Options.DoesNotExist:
             raise Http404



class VoterListAPI(APIView):

    def get(self, request, format=None):
        snippets = VoterList.objects.all()
        serializer = VoterListSerializer(snippets, many=True)
        return Response(serializer.data)


    def post(self, request, format=None):
        receivedData = json.loads(request.body)
        serializer = VoterListSerializer(data=receivedData)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

class CreatedPolls(APIView):

    def get(self, request,userID, format=None):
        allObjects = Poll.objects.filter(createdBy=userID)
        serializer = PollSerializer(allObjects, many=True)
        return Response(serializer.data)
class CastVotes(APIView):

    def get(self, request,userID, format=None):
        try:
            voters = VoterList.objects.filter(isVoteCasted=False).filter(userID=userID)

            allPollsToExclude =Poll.objects.all()

            for voter in voters:
                try:
                    allPollsToExclude=allPollsToExclude.exclude(id=voter.pollID.id)
                except Exception:
                    pass

            allPollsToInclude =Poll.objects.all()

            for exclude in allPollsToExclude:
                try:
                    allPollsToInclude = allPollsToInclude.exclude(id=exclude.id)
                except Exception:
                    pass

            serializer = PollSerializer(allPollsToInclude, many=True)
        except Exception:
            serializer = PollSerializer(None, many=True)

        return Response(serializer.data)
class CastedVotes(APIView):

    def get(self, request,userID, format=None):
        voters = VoterList.objects.filter(isVoteCasted=True).filter(userID=userID)
        allPollsToExclude =Poll.objects.all()
        for voter in voters:
            allPollsToExclude=allPollsToExclude.exclude(id=voter.pollID.id)
        allPollsToInclude =Poll.objects.all()
        for exclude in allPollsToExclude:
            allPollsToInclude = allPollsToInclude.exclude(id=exclude.id)

        serializer = PollSerializer(allPollsToInclude,many=True)
        return Response(serializer.data)

class UpdatePoll(APIView):
    def put(self, request, pk, format=None):
        recievedData = json.loads(request.body)
        startTime = dateutil.parser.parse(recievedData['startTime'])
        startTime = startTime.replace(tzinfo=None)
        obj = Poll.objects.get(id=pk)
        obj.title = recievedData['title']
        obj.question_outline = recievedData['question_outline']
        obj.description = recievedData['description']
        obj.startTime = recievedData['startTime']
        obj.endTime = recievedData['endTime']
        if startTime > datetime.datetime.now():
            obj.pollCriteria_emails = recievedData['pollCriteria_emails']
            obj.pollCriteria_domain = recievedData['pollCriteria_domain']
            allObjects = VoterList.objects.filter(pollID=pk)
            allObjects.delete()
        obj.save()
        isAdminAllowedForVoting = recievedData['isAdminAllowedForVoting']
        if isAdminAllowedForVoting == "TRUE":
            voter = {
                "isVoteCasted": False,
                "optionID": None,
                "pollID": pk,
                "userID": recievedData['createdBy']}
            serializer = VoterListSerializer(data=voter)
            if serializer.is_valid():
                serializer.save()
            else:
                return False
        if recievedData['pollCriteria_emails'] != "" and startTime > datetime.datetime.now():
            emails = str(recievedData['pollCriteria_emails']).split(',')
            for email in emails:
                try:
                    verificationID = EmailVerification.objects.get(email=email).pk
                    userID = User.objects.get(email=verificationID).id
                except:
                    continue
                try:
                    alreadyExistRecord = VoterList.objects.filter(pollID=recievedData['id']).get(userID=userID)
                    continue
                except:
                    pass
                voter = {
                    "isVoteCasted": False,
                    "optionID": None,
                    "pollID": recievedData['id'],
                    "userID": userID}
                if obj.createdBy.pk != userID:
                    serializer = VoterListSerializer(data=voter)
                    if serializer.is_valid():
                        serializer.save()
                    else:
                        return False
        if recievedData['pollCriteria_domain'] != "" and startTime > datetime.datetime.now():
            emailDomains = str(recievedData['pollCriteria_domain']).split(',')
            allObjects = EmailVerification.objects.all()
            for domain in emailDomains:
                for obj in allObjects:
                    if domain in obj.email:
                        try:
                            userID = User.objects.get(email=obj.id).id
                        except:
                            continue
                            # already exist in voter List
                        try:
                            alreadyExistRecord = VoterList.objects.filter(pollID=recievedData['id']).get(userID=userID)
                            continue
                        except:
                            pass
                        voter = {
                            "isVoteCasted": False,
                            "optionID": None,
                            "pollID": recievedData['id'],
                            "userID": userID}
                        if Poll.objects.get(id=pk).createdBy.pk != userID:
                            serializer = VoterListSerializer(data=voter)
                            if serializer.is_valid():
                                serializer.save()
                            else:
                                return False
        if recievedData['pollCriteria_phoneNo'] != "" and startTime > datetime.datetime.now():
            phoneNos = str(recievedData['pollCriteria_phoneNo']).split(',')
            allObjects = PhoneNoVerification.objects.all()
            for phNo in phoneNos:
                for obj in allObjects:
                    if phNo in obj.phoneNo:
                        try:
                            userID = User.objects.get(phoneNo=obj.id).id
                        except:
                            continue
                            # already exist in voter List
                        try:
                            alreadyExistRecord = VoterList.objects.filter(pollID=recievedData['id']).get(userID=userID)
                            continue
                        except:
                            pass
                        voter = {
                            "isVoteCasted": False,
                            "optionID": None,
                            "pollID": recievedData['id'],
                            "userID": userID}
                        if Poll.objects.get(id=pk).createdBy.pk != userID:
                            serializer = VoterListSerializer(data=voter)
                            if serializer.is_valid():
                                serializer.save()
                            else:
                                return False
        return Response()
#shahwar work

class Dashboard(APIView):

    def get(self, request, format=None):
        allObjects = Notification.objects.all()
        serializer = NotificationSerializer(allObjects, many=True)
        return Response(serializer.data)


class PollAddNotification(APIView):
    def get(self, request, uId):
        pollEndNotification(uId)
        pollEndStats(uId)

        allObjects = Notification.objects.filter(userId=uId)
        for obj in allObjects:
            dateObj = dateutil.parser.parse(obj.createdOn)
            nowObj = datetime.datetime.now()
            if (nowObj-dateObj).days > 0:
                allObjects.filter(userId=obj.userId).delete()
        notifyArr = []
        allObjects = Notification.objects.filter(userId=uId)
        for obj in allObjects:
            pollObj = Poll.objects.get(id=obj.pollId)
            if pollObj.createdBy == int(uId):
                notifyObj = {
                    'userId': obj.userId,
                    'type': obj.type,
                    'pollId': obj.pollId,
                    'pollType': 'createdPolls',
                    'name':pollObj.title,
                    'createdOn': obj.createdOn
                }
                notifyArr.append(notifyObj)
            elif VoterList.objects.filter(userID=uId).get(pollID=obj.pollId).isVoteCasted == 1:
                notifyObj = {
                    'userId': obj.userId,
                    'type': obj.type,
                    'pollId': obj.pollId,
                    'pollType': 'castedVotePolls',
                    'name': pollObj.title,
                    'createdOn': obj.createdOn
                }
                notifyArr.append(notifyObj)
            else:
                notifyObj = {
                    'userId': obj.userId,
                    'type': obj.type,
                    'pollId': obj.pollId,
                    'pollType': 'castVotePolls',
                    'name': pollObj.title,
                    'createdOn': obj.createdOn
                }
                notifyArr.append(notifyObj)
        return Response(notifyArr)



def pollEndNotification(uId):
    allListObjects = VoterList.objects.filter(userID=uId).filter(isVoteCasted=0)
    for listObj in allListObjects:
        allObjects = Poll.objects.filter(id=listObj.pollID.id)
        for obj in allObjects:
            pollEndTime = obj.endTime
            pollStartTime = obj.startTime
            pollStartTime = pollStartTime.replace(tzinfo=None)
            pollEndTime = pollEndTime.replace(tzinfo=None)
            minutes = round((pollEndTime - datetime.datetime.now()).total_seconds() / 60)
            if 5 >= minutes > 0:
                if datetime.datetime.now() >= pollStartTime:
                    allPollEndObjects = Notification.objects.filter(pollId=obj.id).filter(type='pollEnd').filter(
                        userId=uId)
                    if allPollEndObjects.count() == 0:
                        notifyObj = {
                            "userId": uId,
                            "type": "pollEnd",
                            "pollId": obj.id,
                            "createdOn": datetime.datetime.now().isoformat()
                        }
                        notifySerializer = NotificationSerializer(data=notifyObj)
                        if notifySerializer.is_valid():
                            notifySerializer.save()


def pollEndStats(uId):
    allListObjects = VoterList.objects.filter(userID=uId)
    for listObj in allListObjects:
        allObjects = Poll.objects.filter(id=listObj.pollID.id)
        for obj in allObjects:
            pollEndTime = obj.endTime
            pollEndTime = pollEndTime.replace(tzinfo=None)
            minutes = round((datetime.datetime.now() - pollEndTime).total_seconds() / 60)
            if minutes >= 0:
                allPollStatObjects = Notification.objects.filter(pollId=obj.id).filter(type='pollStats').filter(userId=uId)
                if allPollStatObjects.count() == 0:
                    notifyObj = {
                        "userId": uId,
                        "type": "pollStats",
                        "pollId": obj.id,
                        "createdOn": datetime.datetime.now().isoformat()
                    }
                    notifySerializer = NotificationSerializer(data=notifyObj)
                    if notifySerializer.is_valid():
                        notifySerializer.save()
class GetUserStats(APIView):
    def get(self, request, uId):
        obj = Poll.objects.filter(createdBy=uId)
        createdPolls = obj.count()
        activePolls = 0
        endedPolls = 0
        votedPolls = 0
        unVotedPolls = 0
        vlist = VoterList.objects.filter(userID=uId)
        for listObj in vlist:
            if listObj.pollID.endTime.replace(tzinfo=None) > datetime.datetime.now():
                activePolls = activePolls + 1
        for listObj in vlist:
            if listObj.pollID.endTime.replace(tzinfo=None) < datetime.datetime.now():
                endedPolls = endedPolls + 1
        vlist = VoterList.objects.filter(userID=uId).filter(isVoteCasted=0)
        unVotedPolls = vlist.count()
        vlist = VoterList.objects.filter(userID=uId).filter(isVoteCasted=1)
        votedPolls = vlist.count()
        statsObj = {
            "createdPolls": createdPolls,
            "activePolls": activePolls,
            "endedPolls": endedPolls,
            "votedPolls": votedPolls,
            "unVotedPolls": unVotedPolls
        }
        return Response(statsObj)

class DeleteOptions(APIView):
    def delete (self, request, pollID, format = None):
        try:
            objOptions = Options.objects.filter(pollID=pollID)
            objOptions.delete()
            return Response({'message': 'Options Deleted'})
        except Poll.DoesNotExist:
            return Response({'message': 'Invalid Options ID'})
