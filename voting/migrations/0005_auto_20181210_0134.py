# Generated by Django 2.1.3 on 2018-12-09 20:34

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('voting', '0004_remove_poll_createdby'),
    ]

    operations = [
        migrations.AlterField(
            model_name='poll',
            name='pollCriteria_domain',
            field=models.CharField(max_length=250, null=True),
        ),
        migrations.AlterField(
            model_name='poll',
            name='pollCriteria_emails',
            field=models.CharField(max_length=250, null=True),
        ),
    ]
