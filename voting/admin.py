from django.contrib import admin
from .models import Poll,Options,VoterList

# Register your models here.
admin.site.register(Poll)
admin.site.register(Options)
admin.site.register(VoterList)