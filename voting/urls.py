from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^$', views.index, name='index'),
     path('polls/', views.CreatePoll.as_view()),
     path('deletePoll/<int:pk>/', views.DeletePoll.as_view()),

     path('createdPolls/<int:userID>/', views.CreatedPolls.as_view()),
     path('castVotePolls/<int:userID>/', views.CastVotes.as_view()),
     path('castedVotePolls/<int:userID>/', views.CastedVotes.as_view()),

     path('options/', views.AddOptions.as_view()),
     path('options/<int:pollID>/', views.AddOptions.as_view()),
     path('voterlist/', views.VoterListAPI.as_view()),
     #wasif
     path('updatePoll/<int:pk>/', views.UpdatePoll.as_view()),
     path('deleteOptions/<int:pollID>/', views.DeleteOptions.as_view()),

 #shahwar work
     path('notification/', views.Dashboard.as_view()),
     path('pollAddNotification/<uId>/', views.PollAddNotification.as_view()),
     path('getUserStats/<uId>/', views.GetUserStats.as_view()),

 # url(r'^allRec', views.Verification.as_view()),
   # path('verifyEmail/<slug:email>/', views.VerifyEmail.as_view()),
    #path('verifyEmail/', views.Verification.as_view()),
    #path('registerUser/', views.UserRegistration.as_view()),
]

