-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Dec 12, 2018 at 03:39 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `gvs`
--

-- --------------------------------------------------------

--
-- Table structure for table `auth_group`
--

CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL,
  `name` varchar(80) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_group_permissions`
--

CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_permission`
--

CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_permission`
--

INSERT INTO `auth_permission` (`id`, `name`, `content_type_id`, `codename`) VALUES
(1, 'Can add log entry', 1, 'add_logentry'),
(2, 'Can change log entry', 1, 'change_logentry'),
(3, 'Can delete log entry', 1, 'delete_logentry'),
(4, 'Can view log entry', 1, 'view_logentry'),
(5, 'Can add permission', 2, 'add_permission'),
(6, 'Can change permission', 2, 'change_permission'),
(7, 'Can delete permission', 2, 'delete_permission'),
(8, 'Can view permission', 2, 'view_permission'),
(9, 'Can add group', 3, 'add_group'),
(10, 'Can change group', 3, 'change_group'),
(11, 'Can delete group', 3, 'delete_group'),
(12, 'Can view group', 3, 'view_group'),
(13, 'Can add user', 4, 'add_user'),
(14, 'Can change user', 4, 'change_user'),
(15, 'Can delete user', 4, 'delete_user'),
(16, 'Can view user', 4, 'view_user'),
(17, 'Can add content type', 5, 'add_contenttype'),
(18, 'Can change content type', 5, 'change_contenttype'),
(19, 'Can delete content type', 5, 'delete_contenttype'),
(20, 'Can view content type', 5, 'view_contenttype'),
(21, 'Can add session', 6, 'add_session'),
(22, 'Can change session', 6, 'change_session'),
(23, 'Can delete session', 6, 'delete_session'),
(24, 'Can view session', 6, 'view_session'),
(25, 'Can add dummy', 7, 'add_dummy'),
(26, 'Can change dummy', 7, 'change_dummy'),
(27, 'Can delete dummy', 7, 'delete_dummy'),
(28, 'Can view dummy', 7, 'view_dummy'),
(29, 'Can add email verification', 8, 'add_emailverification'),
(30, 'Can change email verification', 8, 'change_emailverification'),
(31, 'Can delete email verification', 8, 'delete_emailverification'),
(32, 'Can view email verification', 8, 'view_emailverification'),
(33, 'Can add user', 9, 'add_user'),
(34, 'Can change user', 9, 'change_user'),
(35, 'Can delete user', 9, 'delete_user'),
(36, 'Can view user', 9, 'view_user'),
(37, 'Can add voter list', 10, 'add_voterlist'),
(38, 'Can change voter list', 10, 'change_voterlist'),
(39, 'Can delete voter list', 10, 'delete_voterlist'),
(40, 'Can view voter list', 10, 'view_voterlist'),
(41, 'Can add poll', 11, 'add_poll'),
(42, 'Can change poll', 11, 'change_poll'),
(43, 'Can delete poll', 11, 'delete_poll'),
(44, 'Can view poll', 11, 'view_poll'),
(45, 'Can add options', 12, 'add_options'),
(46, 'Can change options', 12, 'change_options'),
(47, 'Can delete options', 12, 'delete_options'),
(48, 'Can view options', 12, 'view_options'),
(49, 'Can add login details', 13, 'add_logindetails'),
(50, 'Can change login details', 13, 'change_logindetails'),
(51, 'Can delete login details', 13, 'delete_logindetails'),
(52, 'Can view login details', 13, 'view_logindetails');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user`
--

CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL,
  `password` varchar(128) NOT NULL,
  `last_login` datetime(6) DEFAULT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(150) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(150) NOT NULL,
  `email` varchar(254) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `auth_user`
--

INSERT INTO `auth_user` (`id`, `password`, `last_login`, `is_superuser`, `username`, `first_name`, `last_name`, `email`, `is_staff`, `is_active`, `date_joined`) VALUES
(1, 'pbkdf2_sha256$120000$NzGBWBjsXn2I$8I3evyZfNRdzWD+BAckum5EzLzzHl5vVqufGpApTxLI=', NULL, 1, 'admin', '', '', 'admin@xyz.com', 1, 1, '2018-12-08 18:38:08.635841');

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_groups`
--

CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `auth_user_user_permissions`
--

CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_admin_log`
--

CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL,
  `action_time` datetime(6) NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) UNSIGNED NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `django_content_type`
--

CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_content_type`
--

INSERT INTO `django_content_type` (`id`, `app_label`, `model`) VALUES
(1, 'admin', 'logentry'),
(3, 'auth', 'group'),
(2, 'auth', 'permission'),
(4, 'auth', 'user'),
(5, 'contenttypes', 'contenttype'),
(7, 'registration', 'dummy'),
(8, 'registration', 'emailverification'),
(13, 'registration', 'logindetails'),
(9, 'registration', 'user'),
(6, 'sessions', 'session'),
(12, 'voting', 'options'),
(11, 'voting', 'poll'),
(10, 'voting', 'voterlist');

-- --------------------------------------------------------

--
-- Table structure for table `django_migrations`
--

CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `django_migrations`
--

INSERT INTO `django_migrations` (`id`, `app`, `name`, `applied`) VALUES
(1, 'contenttypes', '0001_initial', '2018-12-08 18:37:14.504449'),
(2, 'auth', '0001_initial', '2018-12-08 18:37:25.077243'),
(3, 'admin', '0001_initial', '2018-12-08 18:37:27.245160'),
(4, 'admin', '0002_logentry_remove_auto_add', '2018-12-08 18:37:27.289356'),
(5, 'admin', '0003_logentry_add_action_flag_choices', '2018-12-08 18:37:27.344937'),
(6, 'contenttypes', '0002_remove_content_type_name', '2018-12-08 18:37:28.341646'),
(7, 'auth', '0002_alter_permission_name_max_length', '2018-12-08 18:37:29.282352'),
(8, 'auth', '0003_alter_user_email_max_length', '2018-12-08 18:37:30.402179'),
(9, 'auth', '0004_alter_user_username_opts', '2018-12-08 18:37:30.450921'),
(10, 'auth', '0005_alter_user_last_login_null', '2018-12-08 18:37:30.998837'),
(11, 'auth', '0006_require_contenttypes_0002', '2018-12-08 18:37:31.067356'),
(12, 'auth', '0007_alter_validators_add_error_messages', '2018-12-08 18:37:31.116488'),
(13, 'auth', '0008_alter_user_username_max_length', '2018-12-08 18:37:32.028042'),
(14, 'auth', '0009_alter_user_last_name_max_length', '2018-12-08 18:37:33.543350'),
(15, 'registration', '0001_initial', '2018-12-08 18:37:35.307371'),
(16, 'sessions', '0001_initial', '2018-12-08 18:37:36.086771'),
(17, 'voting', '0001_initial', '2018-12-08 20:35:06.570192'),
(18, 'voting', '0002_auto_20181210_0052', '2018-12-09 19:52:42.800265'),
(19, 'voting', '0003_poll_createdby', '2018-12-10 13:39:43.523626'),
(20, 'voting', '0004_remove_poll_createdby', '2018-12-10 13:39:43.603282'),
(21, 'voting', '0005_auto_20181210_0134', '2018-12-10 13:39:43.678640'),
(22, 'voting', '0006_poll_createdby', '2018-12-10 13:39:43.762216'),
(23, 'voting', '0007_remove_poll_createdby', '2018-12-10 13:39:43.793461'),
(24, 'voting', '0008_poll_createdby', '2018-12-10 13:39:43.828480'),
(25, 'voting', '0009_remove_poll_createdby', '2018-12-10 13:39:43.949249'),
(26, 'voting', '0010_poll_createdby', '2018-12-10 13:39:44.013784'),
(27, 'voting', '0011_remove_poll_createdby', '2018-12-10 13:41:23.163079'),
(28, 'voting', '0012_poll_createdby', '2018-12-10 13:41:48.315112'),
(29, 'registration', '0002_logindetails', '2018-12-10 14:37:09.903721'),
(30, 'registration', '0003_logindetails_userid', '2018-12-10 14:43:05.161512');

-- --------------------------------------------------------

--
-- Table structure for table `django_session`
--

CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime(6) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `registration_dummy`
--

CREATE TABLE `registration_dummy` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `registration_emailverification`
--

CREATE TABLE `registration_emailverification` (
  `id` int(11) NOT NULL,
  `email` varchar(50) NOT NULL,
  `generateCode` varchar(50) NOT NULL,
  `verificationStatus` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration_emailverification`
--

INSERT INTO `registration_emailverification` (`id`, `email`, `generateCode`, `verificationStatus`) VALUES
(1, 'usamarajpoot03@yahoo.com', 'ABC', 1),
(3, 'bsef15m015@pucit.edu.pk', '4C0PA', 1),
(4, 'bsef15m041@pucit.edu.pk', 'EGA9R', 1),
(5, 'bsef15m014@pucit.edu.pk', 'QNQ0W', 1);

-- --------------------------------------------------------

--
-- Table structure for table `registration_logindetails`
--

CREATE TABLE `registration_logindetails` (
  `id` int(11) NOT NULL,
  `loginID` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `isVerified` tinyint(1) NOT NULL,
  `message` varchar(50) NOT NULL,
  `userID_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration_logindetails`
--

INSERT INTO `registration_logindetails` (`id`, `loginID`, `password`, `isVerified`, `message`, `userID_id`) VALUES
(1, 'bsef5m015@pucit.edu.pk', '123', 0, 'ok', NULL),
(2, 'bsef5m015@pucit.edu.pk', '123', 0, 'Invalid loginID', NULL),
(3, 'bsef15m015@pucit.edu.pk', '123', 1, 'Details Matched', NULL),
(4, 'bsef15m015@pucit.edu.pk', '1236', 0, 'Invalid password', NULL),
(5, 'bsef15m015@pucit.edu.pk', '1236', 0, 'Invalid password', NULL),
(6, 'bsef15m015@pucit.edu.pk', '123', 1, 'Details Matched', 3),
(7, 'bsef15m014@pucit.edu.pk', '123', 0, 'Invalid password', 3),
(8, 'bsef15014@pucit.edu.pk', '123', 0, 'Invalid loginID', 3),
(9, 'bsef15m014@pucit.edu.pk', 'xyz', 1, 'Details Matched', 6);

-- --------------------------------------------------------

--
-- Table structure for table `registration_user`
--

CREATE TABLE `registration_user` (
  `id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `password` varchar(50) NOT NULL,
  `phoneNo` varchar(50) NOT NULL,
  `email_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `registration_user`
--

INSERT INTO `registration_user` (`id`, `name`, `password`, `phoneNo`, `email_id`) VALUES
(3, 'WASIF', '123', '33333', 3),
(4, 'Usa', '123', '33333', 1),
(5, 'Shahwar', '123', '0300', 4),
(6, 'Ali Nasir', 'xyz', '0300', 5);

-- --------------------------------------------------------

--
-- Table structure for table `voting_options`
--

CREATE TABLE `voting_options` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `description` varchar(250) NOT NULL,
  `voteCount` int(11) NOT NULL,
  `pollID_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voting_options`
--

INSERT INTO `voting_options` (`id`, `title`, `description`, `voteCount`, `pollID_id`) VALUES
(1, 'hey these are options:)', 'oh yeah', 0, 1),
(2, 'hey these are options:)22', 'oh yeah', 0, 1),
(3, 'hey these are options:)22', 'oh yeah', 8, 12),
(4, 'hey these are options:)', 'oh yeah', 1, 1),
(5, 'hey these are options:)', 'oh yeah UPDATED', 2, 2),
(6, 'hey these are options:)', 'oh yeah UPDasdasdATED', 1, 2),
(7, 'hey these are options:)newwwwwwwwwwww', 'oh yeah', 3, 1),
(8, 'hey these are options:)newwwwwwwwwwww', 'oh yeah', 4, 1),
(9, 'hey these are options:)', 'oh yeah', 1, 22),
(10, 'SAAD', 'oh yeah', 0, 23),
(11, 'option2333', 'oh yeah', 1, 23),
(12, 'ALI-option', 'oh yeah', 0, 25),
(13, 'ALI-option-1', 'oh yeah', 1, 25);

-- --------------------------------------------------------

--
-- Table structure for table `voting_poll`
--

CREATE TABLE `voting_poll` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `question_outline` varchar(50) NOT NULL,
  `description` varchar(250) NOT NULL,
  `pollCriteria_domain` varchar(250) DEFAULT NULL,
  `pollCriteria_emails` varchar(250) DEFAULT NULL,
  `startTime` datetime(6) NOT NULL,
  `endTime` datetime(6) NOT NULL,
  `createdBy_id` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voting_poll`
--

INSERT INTO `voting_poll` (`id`, `title`, `question_outline`, `description`, `pollCriteria_domain`, `pollCriteria_emails`, `startTime`, `endTime`, `createdBy_id`) VALUES
(1, 'TestingPoll', 'ohh its working?', 'no description yet/', '@pucit.edu.pk,@bzu.edu.pk', '', '0000-00-00 00:00:00.000000', '0000-00-00 00:00:00.000000', NULL),
(2, 'TestingPollNew', 'ohh its working?', 'no description yet/', '@pucit.edu.pk,@bzu.edu.pk', 'No', '2018-12-08 21:10:19.787393', '2018-12-08 21:10:19.787393', NULL),
(3, 'TestingPollNew', 'ohh its working?', 'no description yet/', '@pucit.edu.pk,@bzu.edu.pk', 'usamarajpoot03@yahoo.com,bsef15m015@pucit.edu.pk', '2018-12-09 20:19:47.867421', '2018-12-09 20:19:47.867421', NULL),
(4, 'TestingPollNew', 'ohh its working?', 'no description yet/', '@pucit.edu.pk,@bzu.edu.pk', 'usamarajpoot03@yahoo.com,bsef15m015@pucit.edu.pk', '2018-12-09 20:21:25.414979', '2018-12-09 20:21:25.414979', NULL),
(5, 'TestingPollNew', 'ohh its working?', 'no description yet/', '@pucit.edu.pk,@bzu.edu.pk', 'usamarajpoot03@yahoo.com,bsef15m015@pucit.edu.pk', '2018-12-09 20:22:01.830775', '2018-12-09 20:22:01.830775', NULL),
(6, 'TestingPollNew', 'ohh its working?', 'no description yet/', '@pucit.edu.pk,@bzu.edu.pk', 'usamarajpoot03@yahoo.com,bsef15m015@pucit.edu.pk', '2018-12-09 20:22:24.100672', '2018-12-09 20:22:24.100672', NULL),
(7, 'TestingPollNew', 'ohh its working?', 'no description yet/', '@pucit.edu.pk,@bzu.edu.pk', 'usamarajpoot03@yahoo.com,bsef15m015@pucit.edu.pk', '2018-12-09 20:22:39.683223', '2018-12-09 20:22:39.683223', NULL),
(8, 'TestingPollNew', 'ohh its working?', 'no description yet/', '@pucit.edu.pk,@bzu.edu.pk', 'usamarajpoot03@yahoo.com,bsef15m015@pucit.edu.pk', '2018-12-09 20:25:51.601027', '2018-12-09 20:25:51.601027', NULL),
(9, 'TestingPollNew', 'ohh its working?', 'no description yet/', '@pucit.edu.pk,@bzu.edu.pk', 'usamarajpoot03@yahoo.com,bsef15m015@pucit.edu.pk', '2018-12-09 20:27:11.507205', '2018-12-09 20:27:11.507205', NULL),
(10, 'TestingPollNew', 'ohh its working?', 'no description yet/', NULL, 'usamarajpoot03@yahoo.com,bsef15m015@pucit.edu.pk', '2018-12-09 20:34:44.972142', '2018-12-09 20:34:44.972142', NULL),
(11, 'TestingPollNew', 'ohh its working?', 'no description yet/', NULL, 'usamarajpoot03@yahoo.com,bsef15m015@pucit.edu.pk', '2018-12-09 20:37:32.175895', '2018-12-09 20:37:32.175895', NULL),
(12, 'TestingPollNew', 'ohh its working?', 'no description yet/', NULL, 'usamarajpoot03@yahoo.com', '2018-12-09 20:37:49.890846', '2018-12-09 20:37:49.890846', NULL),
(13, 'TestingPollNew', 'ohh its working?', 'no description yet/', NULL, NULL, '2018-12-09 20:38:04.020048', '2018-12-09 20:38:04.020048', NULL),
(14, 'TestingPollNew', 'ohh its working?', 'no description yet/', '@yahoo.com,@pucit.edu.pk', NULL, '2018-12-09 20:44:34.790291', '2018-12-09 20:44:34.790291', NULL),
(15, 'TestingPollNew', 'ohh its working?', 'no description yet/', '@yahoo.com,@pucit.edu.pk', NULL, '2018-12-09 20:48:32.409495', '2018-12-09 20:48:32.409495', NULL),
(16, 'TestingPollNew', 'ohh its working?', 'no description yet/', '@yahoo.com,@pucit.edu.pk', NULL, '2018-12-09 20:48:45.365644', '2018-12-09 20:48:45.365644', NULL),
(17, 'TestingPollNew', 'ohh its working?', 'no description yet/', '@yahoo.com,@pucit.edu.pk', NULL, '2018-12-09 20:49:06.821895', '2018-12-09 20:49:06.821895', NULL),
(18, 'TestingPollNew', 'ohh its working?', 'no description yet/', '@yahoo.com,@pucit.edu.pk', NULL, '2018-12-09 20:50:09.192964', '2018-12-09 20:50:09.192964', NULL),
(19, 'TestingPollNew', 'ohh its working?', 'no description yet/', '@yahoo.com,@pucit.edu.pk', NULL, '2018-12-09 20:50:52.721763', '2018-12-09 20:50:52.721763', NULL),
(20, 'TestingPollNew', 'ohh its working?', 'no description yet/', '@yahoo.com,@pucit.edu.pk', NULL, '2018-12-09 20:51:22.041327', '2018-12-09 20:51:22.041327', NULL),
(21, 'TestingPollNew', 'ohh its working?', 'no description yet/', '@yahoo.com,@pucit.edu.pk', NULL, '2018-12-09 20:51:46.834906', '2018-12-09 20:51:46.834906', NULL),
(22, 'TestingPollNew', 'ohh its working?', 'no description yet/', '@yahoo.com,@pucit.edu.pk', NULL, '2018-12-09 20:52:16.193160', '2018-12-09 20:52:16.193160', NULL),
(23, 'SHAHWAR POLL', 'is my QUESTION', 'no description yet/', '@yahoo.com,@pucit.edu.pk', NULL, '2018-12-10 11:32:50.591532', '2018-12-10 11:32:50.591532', NULL),
(24, 'SHAHWAR POLL', 'is my QUESTION', 'no description yet/', '@yahoo.com,@pucit.edu.pk', NULL, '2018-12-10 13:44:00.171985', '2018-12-10 13:44:00.171985', 5),
(25, 'ALI POLL', 'is my QUESTION', 'no description yet/', '@pucit.edu.pk', NULL, '2018-12-10 15:04:48.192964', '2018-12-10 15:04:48.192964', 5);

-- --------------------------------------------------------

--
-- Table structure for table `voting_voterlist`
--

CREATE TABLE `voting_voterlist` (
  `id` int(11) NOT NULL,
  `isVoteCasted` tinyint(1) NOT NULL,
  `optionID_id` int(11) DEFAULT NULL,
  `pollID_id` int(11) NOT NULL,
  `userID_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `voting_voterlist`
--

INSERT INTO `voting_voterlist` (`id`, `isVoteCasted`, `optionID_id`, `pollID_id`, `userID_id`) VALUES
(19, 0, NULL, 9, 4),
(20, 0, NULL, 9, 3),
(21, 0, NULL, 10, 4),
(22, 0, NULL, 10, 3),
(23, 0, NULL, 11, 4),
(24, 0, NULL, 11, 3),
(25, 1, 3, 12, 3),
(26, 0, NULL, 22, 4),
(27, 1, 9, 22, 3),
(28, 1, 3, 3, 3),
(30, 0, NULL, 23, 4),
(31, 0, NULL, 23, 3),
(32, 1, 11, 23, 5),
(33, 0, NULL, 24, 4),
(34, 0, NULL, 24, 3),
(35, 0, NULL, 24, 5),
(36, 0, NULL, 25, 3),
(37, 1, 13, 25, 5),
(38, 0, NULL, 25, 6);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `auth_group`
--
ALTER TABLE `auth_group`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `name` (`name`);

--
-- Indexes for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_group_permissions_group_id_permission_id_0cd325b0_uniq` (`group_id`,`permission_id`),
  ADD KEY `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_permission_content_type_id_codename_01ab375a_uniq` (`content_type_id`,`codename`);

--
-- Indexes for table `auth_user`
--
ALTER TABLE `auth_user`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `username` (`username`);

--
-- Indexes for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_groups_user_id_group_id_94350c0c_uniq` (`user_id`,`group_id`),
  ADD KEY `auth_user_groups_group_id_97559544_fk_auth_group_id` (`group_id`);

--
-- Indexes for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `auth_user_user_permissions_user_id_permission_id_14a6b632_uniq` (`user_id`,`permission_id`),
  ADD KEY `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` (`permission_id`);

--
-- Indexes for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD PRIMARY KEY (`id`),
  ADD KEY `django_admin_log_content_type_id_c4bce8eb_fk_django_co` (`content_type_id`),
  ADD KEY `django_admin_log_user_id_c564eba6_fk_auth_user_id` (`user_id`);

--
-- Indexes for table `django_content_type`
--
ALTER TABLE `django_content_type`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `django_content_type_app_label_model_76bd3d3b_uniq` (`app_label`,`model`);

--
-- Indexes for table `django_migrations`
--
ALTER TABLE `django_migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `django_session`
--
ALTER TABLE `django_session`
  ADD PRIMARY KEY (`session_key`),
  ADD KEY `django_session_expire_date_a5c62663` (`expire_date`);

--
-- Indexes for table `registration_dummy`
--
ALTER TABLE `registration_dummy`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration_emailverification`
--
ALTER TABLE `registration_emailverification`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `registration_logindetails`
--
ALTER TABLE `registration_logindetails`
  ADD PRIMARY KEY (`id`),
  ADD KEY `registration_loginde_userID_id_63676dc0_fk_registrat` (`userID_id`);

--
-- Indexes for table `registration_user`
--
ALTER TABLE `registration_user`
  ADD PRIMARY KEY (`id`),
  ADD KEY `registration_user_email_id_32423d9a_fk_registrat` (`email_id`);

--
-- Indexes for table `voting_options`
--
ALTER TABLE `voting_options`
  ADD PRIMARY KEY (`id`),
  ADD KEY `voting_options_pollID_id_27751984_fk_voting_poll_id` (`pollID_id`);

--
-- Indexes for table `voting_poll`
--
ALTER TABLE `voting_poll`
  ADD PRIMARY KEY (`id`),
  ADD KEY `voting_poll_createdBy_id_d1cab118_fk_registration_user_id` (`createdBy_id`);

--
-- Indexes for table `voting_voterlist`
--
ALTER TABLE `voting_voterlist`
  ADD PRIMARY KEY (`id`),
  ADD KEY `voting_voterlist_pollID_id_78c246a9_fk_voting_poll_id` (`pollID_id`),
  ADD KEY `voting_voterlist_userID_id_aff8a439_fk_registration_user_id` (`userID_id`),
  ADD KEY `voting_voterlist_optionID_id_a32df97f_fk_voting_options_id` (`optionID_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `auth_group`
--
ALTER TABLE `auth_group`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_permission`
--
ALTER TABLE `auth_permission`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=53;

--
-- AUTO_INCREMENT for table `auth_user`
--
ALTER TABLE `auth_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `django_content_type`
--
ALTER TABLE `django_content_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `django_migrations`
--
ALTER TABLE `django_migrations`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=31;

--
-- AUTO_INCREMENT for table `registration_dummy`
--
ALTER TABLE `registration_dummy`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `registration_emailverification`
--
ALTER TABLE `registration_emailverification`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;

--
-- AUTO_INCREMENT for table `registration_logindetails`
--
ALTER TABLE `registration_logindetails`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT for table `registration_user`
--
ALTER TABLE `registration_user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- AUTO_INCREMENT for table `voting_options`
--
ALTER TABLE `voting_options`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `voting_poll`
--
ALTER TABLE `voting_poll`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT for table `voting_voterlist`
--
ALTER TABLE `voting_voterlist`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `auth_group_permissions`
--
ALTER TABLE `auth_group_permissions`
  ADD CONSTRAINT `auth_group_permissio_permission_id_84c5c92e_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_group_permissions_group_id_b120cbf9_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`);

--
-- Constraints for table `auth_permission`
--
ALTER TABLE `auth_permission`
  ADD CONSTRAINT `auth_permission_content_type_id_2f476e4b_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`);

--
-- Constraints for table `auth_user_groups`
--
ALTER TABLE `auth_user_groups`
  ADD CONSTRAINT `auth_user_groups_group_id_97559544_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  ADD CONSTRAINT `auth_user_groups_user_id_6a12ed8b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `auth_user_user_permissions`
--
ALTER TABLE `auth_user_user_permissions`
  ADD CONSTRAINT `auth_user_user_permi_permission_id_1fbb5f2c_fk_auth_perm` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`),
  ADD CONSTRAINT `auth_user_user_permissions_user_id_a95ead1b_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `django_admin_log`
--
ALTER TABLE `django_admin_log`
  ADD CONSTRAINT `django_admin_log_content_type_id_c4bce8eb_fk_django_co` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`),
  ADD CONSTRAINT `django_admin_log_user_id_c564eba6_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`);

--
-- Constraints for table `registration_logindetails`
--
ALTER TABLE `registration_logindetails`
  ADD CONSTRAINT `registration_loginde_userID_id_63676dc0_fk_registrat` FOREIGN KEY (`userID_id`) REFERENCES `registration_user` (`id`);

--
-- Constraints for table `registration_user`
--
ALTER TABLE `registration_user`
  ADD CONSTRAINT `registration_user_email_id_32423d9a_fk_registrat` FOREIGN KEY (`email_id`) REFERENCES `registration_emailverification` (`id`);

--
-- Constraints for table `voting_options`
--
ALTER TABLE `voting_options`
  ADD CONSTRAINT `voting_options_pollID_id_27751984_fk_voting_poll_id` FOREIGN KEY (`pollID_id`) REFERENCES `voting_poll` (`id`);

--
-- Constraints for table `voting_poll`
--
ALTER TABLE `voting_poll`
  ADD CONSTRAINT `voting_poll_createdBy_id_d1cab118_fk_registration_user_id` FOREIGN KEY (`createdBy_id`) REFERENCES `registration_user` (`id`);

--
-- Constraints for table `voting_voterlist`
--
ALTER TABLE `voting_voterlist`
  ADD CONSTRAINT `voting_voterlist_optionID_id_a32df97f_fk_voting_options_id` FOREIGN KEY (`optionID_id`) REFERENCES `voting_options` (`id`),
  ADD CONSTRAINT `voting_voterlist_pollID_id_78c246a9_fk_voting_poll_id` FOREIGN KEY (`pollID_id`) REFERENCES `voting_poll` (`id`),
  ADD CONSTRAINT `voting_voterlist_userID_id_aff8a439_fk_registration_user_id` FOREIGN KEY (`userID_id`) REFERENCES `registration_user` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
