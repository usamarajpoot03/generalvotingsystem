from rest_framework import serializers
from .models import EmailVerification, User, LoginDetails, Messages,PhoneNoVerification


class EmailVerificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmailVerification
        fields = ('email', 'generateCode','verificationStatus')
        #fields = '__all__'

class PhoneNoVerificationSerializer(serializers.ModelSerializer):
    class Meta:
        model = PhoneNoVerification
        fields = ('phoneNo', 'generateCode','verificationStatus')
        #fields = '__all__'

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = '__all__'

class LoginDetailsSerializer(serializers.ModelSerializer):
    class Meta:
        model=LoginDetails
        fields = '__all__'

class MessageSerializer(serializers.ModelSerializer):
    class Meta:
        model = Messages
        fields = ('email', 'name', 'message')


class MessageWriteSerializer(serializers.ModelSerializer):
    class Meta:
        model = Messages
        fields = '__all__'
