from django.contrib import admin
from django.urls import path
from django.conf.urls import url
from . import views
urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^allRec', views.Verification.as_view()),
   # path('verifyEmail/<slug:email>/', views.VerifyEmail.as_view()),
    path('verifyEmail/', views.Verification.as_view()),
    path('verifyPhoneNo/', views.VerificationPhoneNo.as_view()),

    path('registerUser/', views.UserRegistration.as_view()),
    path('verifyUser/', views.VerifyLoginDetails.as_view()),

    path('getUser/<int:pk>', views.getUser.as_view()),
    path('updateProfile/<int:pk>', views.UpdateProfile.as_view()),
    path('contact/', views.Contact.as_view()),
#comments
]