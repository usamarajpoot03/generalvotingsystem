import datetime

from django.db import models

class Dummy(models.Model):
    email = models.CharField(max_length=50)

class EmailVerification(models.Model):
    email = models.CharField(max_length=50)
    generateCode = models.CharField(max_length=50)
    verificationStatus = models.BooleanField(default=False)
    def __str__(self):
        return self.email +"\nSTATUS : y"+ self.verificationStatus

class PhoneNoVerification(models.Model):
    phoneNo = models.CharField(max_length=50)
    generateCode = models.CharField(max_length=50)
    verificationStatus = models.BooleanField(default=False)
    def __str__(self):
        return self.phoneNo +"\nSTATUS : y"+ self.verificationStatus



class User(models.Model):
    name=models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    #phoneNo = models.CharField(max_length=50)
    email = models.ForeignKey(EmailVerification, on_delete=models.DO_NOTHING, null=True)
    phoneNo = models.ForeignKey(PhoneNoVerification, on_delete=models.DO_NOTHING, null=True)

class LoginDetails(models.Model):
    loginID=models.CharField(max_length=50)
    password = models.CharField(max_length=50)
    isVerified = models.BooleanField(default=False)
    message=models.CharField(max_length=50)
    userID= models.ForeignKey(User, on_delete=models.DO_NOTHING,null=True)
class Messages(models.Model):
    name = models.CharField(max_length=50, blank=False)
    email = models.CharField(max_length=50, blank=False)
    message = models.CharField(max_length=500)
    postedOn = models.CharField(max_length=50)
    postedBy = models.IntegerField()

    def __str__(self):
        return self.id + "--" + self.title + "--"



